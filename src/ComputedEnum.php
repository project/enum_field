<?php

namespace Drupal\enum_field;

use Drupal\Core\TypedData\TypedData;

/**
 * Computed enum property class.
 */
class ComputedEnum extends TypedData {

  /**
   * The data value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * Computed enum.
   *
   * @var \BackedEnum|null
   */
  protected ?\BackedEnum $enum = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->enum !== NULL) {
      return $this->enum;
    }

    $enumClass = $this->getParent()->getDataDefinition()->getSetting('enum_class');
    $value = $this->getParent()->value;

    if ($value instanceof $enumClass) {
      return $this->enum = $value;
    }

    if ((is_int($value) || is_string($value)) && enum_exists($enumClass) && method_exists($enumClass, 'tryFrom')) {
      return $this->enum = $enumClass::tryFrom($value);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->enum = NULL;
    parent::setValue($value, $notify);
  }

}
