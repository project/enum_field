<?php

namespace Drupal\enum_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\enum_field\ComputedEnum;

/**
 * Common methods for enum field types.
 */
trait EnumItemTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();
    $settings['enum_class'] = '';

    unset($settings['allowed_values']);
    unset($settings['allowed_values_function']);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    return self::getOptions($this->getSetting('enum_class'));
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['enum_class'] = [
      '#type' => 'textfield',
      '#title' => t('Enum class'),
      '#default_value' => $this->getSetting('enum_class'),
      '#description' => t('Backed enum class to get the allowed values from.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (is_array($values)) {
      foreach ($values as &$value) {
        if ($value instanceof \BackedEnum) {
          $value = $value->value;
        }
      }
    }

    if ($values instanceof \BackedEnum) {
      $values = $values->value;
    }

    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $allowed_options = self::getOptions($field_definition->getSetting('enum_class'));
    $values = [];
    $values['value'] = array_rand($allowed_options);

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['enum'] = DataDefinition::create('any')
      ->setLabel(t('Enum'))
      ->setDescription(t('An instance of the enum or enum-like class.'))
      ->setComputed(TRUE)
      ->setClass(ComputedEnum::class);

    return $properties;
  }

  /**
   * Extract an array of allowed values from the enum class.
   */
  public static function getOptions(string $enumClass): array {
    if (!enum_exists($enumClass) || !is_a($enumClass, \BackedEnum::class, TRUE)) {
      return [];
    }

    return array_reduce(
      $enumClass::cases(),
      function (array $options, \BackedEnum $enum): array {
        if (method_exists($enum, 'label')) {
          $label = $enum->label();
        }
        else {
          $label = $enum->name;
        }
        $options[$enum->value] = $label;
        return $options;
      },
      []
    );
  }

}
