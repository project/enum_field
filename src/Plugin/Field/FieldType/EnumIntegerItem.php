<?php

namespace Drupal\enum_field\Plugin\Field\FieldType;

use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;

/**
 * Plugin implementation of the 'enum_integer' field type.
 *
 * @FieldType(
 *   id = "enum_integer",
 *   label = @Translation("Enum (integer)"),
 *   description = {
 *     @Translation("Values stored are numbers without decimals"),
 *     @Translation("Keys and values are defined by the chosen enum."),
 *   },
 *   category = "selection_list",
 *   default_widget = "options_select",
 *   default_formatter = "list_default",
 *   list_class = "\Drupal\enum_field\Plugin\Field\FieldType\EnumItemList",
 * )
 */
class EnumIntegerItem extends ListIntegerItem {

  use EnumItemTrait;

}
