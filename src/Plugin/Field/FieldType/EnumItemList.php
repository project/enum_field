<?php

namespace Drupal\enum_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemList;

/**
 * Defines an item list class for enum fields.
 */
class EnumItemList extends FieldItemList {

  /**
   * Gets the enums referenced by this field, preserving field item deltas.
   *
   * @return \BackedEnum[]
   *   An array of enum objects keyed by field item deltas.
   */
  public function enums(): array {
    return array_map(
      fn (FieldItemInterface $item) => $item->enum,
      $this->list
    );
  }

}
