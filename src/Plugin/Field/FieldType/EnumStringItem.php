<?php

namespace Drupal\enum_field\Plugin\Field\FieldType;

use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Plugin implementation of the 'enum_string' field type.
 *
 * @FieldType(
 *   id = "enum_string",
 *   label = @Translation("Enum (text)"),
 *   description = {
 *     @Translation("Values stored are text values"),
 *     @Translation("Keys and values are defined by the chosen enum."),
 *   },
 *   category = "selection_list",
 *   default_widget = "options_select",
 *   default_formatter = "list_default",
 *   list_class = "\Drupal\enum_field\Plugin\Field\FieldType\EnumItemList",
 * )
 */
class EnumStringItem extends ListStringItem {

  use EnumItemTrait;

}
