<?php

namespace Drupal\enum_field\Exception;

/**
 * Exception thrown when an invalid enum is used.
 */
class InvalidEnumException extends \InvalidArgumentException {
}
